import click
import h5py as h5
import numpy as np
import plx
from pathlib import Path
from tempfile import TemporaryDirectory
from tqdm import tqdm


@click.command()
@click.argument('input-path', type=click.Path(exists=True, readable=True, dir_okay=True))
@click.argument('output-path', type=click.Path(writable=True, dir_okay=False))
def main(input_path: str, output_path: str):
    input_path = Path(input_path)
    output_path = Path(output_path)

    with TemporaryDirectory() as export_dir:
        if input_path.is_dir():
            explx = plx.ExportedPlxFile(input_path)
        else:
            Path(export_dir).mkdir(parents=True, exist_ok=True)
            explx = plx.PlxFile(input_path, export_base_dir=export_dir).export_file()

        no_channels = guess_no_channels(explx)

        with h5.File(output_path, 'w') as h5file:
            h5file.attrs['ad_frequency'] = explx.header['ADFrequency']

            for i, spike_header in enumerate(tqdm(explx.spike_headers)):
                spike_data = explx.spike_channel(spike_header.channel).data[['timestamp', 'waveform', 'unit']]
                if i < no_channels:
                    export_spike(h5file, explx.header, spike_header, spike_data)    
            
            for slow_header in tqdm(explx.slow_headers):
                slow_data = explx.slow_channel(slow_header.channel).data
                if len(slow_data):
                    export_slow(h5file, explx.header, slow_header, slow_data)


def guess_no_channels(explx: plx.ExportedPlxFile) -> int:
    spike_headers = explx.spike_headers
    slow_headers = explx.slow_headers

    no_spike_headers = 0
    for spike_header in spike_headers:
        spike_data = explx.spike_channel(spike_header.channel).data
        if len(spike_data) > 0:
            if no_spike_headers == 0:
                no_spike_headers = 1
            elif spike_header.channel > no_spike_headers:
                no_spike_headers *= 2

    no_slow_headers = 0
    for i, slow_header in enumerate(slow_headers):
        slow_data = explx.slow_channel(i).data
        if len(slow_data) > 0 and slow_header.spike_channel > no_slow_headers:
            no_slow_headers = slow_header.spike_channel

    return max(no_spike_headers, no_slow_headers)


def export_slow(h5file: h5.File, main_header: plx.parsing.Header, slow_header: plx.parsing.SlowHeader, slow_data: np.ndarray):
    slow_dataset = h5file.create_dataset(f"slow/{slow_header.name}", slow_data.shape, slow_data.dtype, chunks=True, shuffle=True, compression='gzip', compression_opts=4)
    slow_dataset.attrs['denorm'] = slow_denorm_factor(main_header, slow_header)
    slow_dataset[:] = slow_data


def export_spike(h5file: h5.File, main_header: plx.parsing.Header, spike_header: plx.parsing.SpikeHeader, spike_data: np.ndarray):
    spike_dataset = h5file.create_dataset(f"spike/{spike_header.name}", spike_data.shape, spike_data.dtype, chunks=True, shuffle=True, compression='gzip', compression_opts=4)
    spike_dataset.attrs['denorm'] = spike_denorm_factor(main_header, spike_header)
    spike_dataset[:] = spike_data


def slow_denorm_factor(main_header: plx.parsing.Header, slow_header: plx.parsing.SlowHeader) -> float:
    plx_version = main_header['Version']
    
    if plx_version < 102:
        max_magnitude = 5000
        digital_levels = 2048
        preamp_gain = 1000
    elif plx_version < 103:
        max_magnitude = 5000
        digital_levels = 2048
        preamp_gain = slow_header['PreAmpGain']
    else:
        max_magnitude = main_header['SlowMaxMagnitudeMV']
        digital_levels = 2**(main_header['BitsPerSlowSample'] - 1)
        preamp_gain = slow_header['PreAmpGain']
        
    factor = max_magnitude / (digital_levels * preamp_gain * slow_header['Gain'])
    return factor


def spike_denorm_factor(main_header: plx.parsing.Header, spike_header: plx.parsing.SpikeHeader) -> float:
    plx_version = main_header['Version']
    
    if plx_version < 103:
        max_magnitude = 3000
        digital_levels = 2048
        preamp_gain = 1000
    elif plx_version < 105:
        max_magnitude = main_header['SpikeMaxMagnitudeMV']
        digital_levels = 2**(main_header['BitsPerSpikeSample'] - 1)
        preamp_gain = 1000
    else:
        max_magnitude = main_header['SpikeMaxMagnitudeMV']
        digital_levels = 2**(main_header['BitsPerSpikeSample'] - 1)
        preamp_gain = main_header['SpikePreAmpGain']
        
    factor = max_magnitude / (digital_levels * preamp_gain * spike_header['Gain'])
    return factor


if __name__ == "__main__":
    main()
